## Stretnutie doktorandov 

  
# @size[55px](Michal Ševčík) 

@color[grey](@size[30px](Born as geek, trained as environmental scientist))
  
&nbsp;

#### 04. 12. 2018

---

# [Zoznam vybraných publikácií](https://gitlab.com/SevcikM/pres/blob/preszoznam/README.md)


--- 
- @size[55%](**#Inquiry Based and Blended Learning Using Geographical Information System**)
- @size[55%](Parallel Computing of Visibility Analysis Directed by Students)
- @size[55%](**#Model of higher GIS education**)
- @size[55%](A New Dimension in Analyses of Availability for Purposes of Tourism)
- @size[55%](**#Diet of shrews *Soricidae* in urban environment**)
- @size[55%](Analyses of availability using GIS taking into consideration of real terrain)
- @size[55%](The computation of real area using GIS demonstrated on management of invasive plants)
- @size[55%](***#Using the GRASS GIS in the technological development and manufacture of insulators***)
- @size[55%](***#Methods of Ivestigating Technological Texture of Ceramic Blanks***)
- @size[55%](***#Changes of small mammal communities with the altitude gradient in Eastern Tatras***)
- @size[55%](Storage Effect On Salt Content And Water Activity Of Parenica Cheese Made From Cows´Milk)
- @size[55%](Roosting in the forest, still hunt in the open land: Diet of Long-eared Owl Asio otus from winter-roosts situated within forest and residential area)
- @size[55%](***#A European-wide high-resolution distribution model for Muscardinus avellanarius***)
---

@snap[north span-80]
@quote[I'm just a ~~musical~~ **publishing** prostitute, my dear!](Freddie Mercury (edited))
@snapend

@snap[south span-20]
![Freddie](https://pre00.deviantart.net/6878/th/pre/f/2011/285/d/d/freddie_mercury_by_rober_raik-d4clrdy.png)
@snapend


--- 

- @size[55%](**#Inquiry Based and Blended Learning Using Geographical Information System**)
- @size[55%](Parallel Computing of Visibility Analysis Directed by Students)
- @size[55%](**#Model of higher GIS education**)
- @size[55%](A New Dimension in Analyses of Availability for Purposes of Tourism)
- @size[55%](**#Diet of shrews *Soricidae* in urban environment**)
- @size[55%](Analyses of availability using GIS taking into consideration of real terrain)
- @size[55%](The computation of real area using GIS demonstrated on management of invasive plants)
- @size[55%](***#Using the GRASS GIS in the technological development and manufacture of insulators***)
- @size[55%](***#Methods of Ivestigating Technological Texture of Ceramic Blanks***)
- @size[55%](***#Changes of small mammal communities with the altitude gradient in Eastern Tatras***)
- @size[55%](Storage Effect On Salt Content And Water Activity Of Parenica Cheese Made From Cows´Milk)
- @size[55%](Roosting in the forest, still hunt in the open land: Diet of Long-eared Owl Asio otus from winter-roosts situated within forest and residential area)
- @size[55%](***#A European-wide high-resolution distribution model for Muscardinus avellanarius***)


---

## Základné metódy vedeckého výskumu (PhD. survival kit&trade;)

---

##### Výskum

@ul
- Zásady publikovania:
  - Autorstvo
  - Názov, abstrakt, kľúčové slová, intro....
  - Iné: výber článkov, [grafy](https://matplotlib.org/users/plotting/colormaps/grayscale_01_05.png), forma....
- Publikačný proces:
  - Peer review, Pre-submission inquiry, Your paper your way, ...
  - Cover letter, Summary of conclusions
- E-maily
- Zásady prezentácie:
  - Ústna / Poster
- Redakčné rady / recenzie
- Iné: DoE, citačné indexy, slovnik
- Reprodukovateľnosť, ...
@ulend

---

#### Administratíva

@ul
- Kategórie výstupov
- Cesťáky / letenky
- Študijné pobyty / Stáže
- Projekty  
- Povolenia k výskumu
- Všeobecné info
- ...
@ulend


---

### Ďakujem za pozornosť





